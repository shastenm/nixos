# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

    nixpkgs.config.permittedInsecurePackages = [
       "python-2.7.18.6"
       "checkinstall-1.6.2"
    ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "nixos"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Enable flatpaks
  xdg.portal.extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
  services.flatpak.enable = true;

  # Enable mlocate
  services.locate.enable = true;
  services.locate.locate = pkgs.mlocate;




  # Set your time zone.
  time.timeZone = "America/Mexico_City";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  # Enable the X11 windowing system.
  services.xserver.enable = true;
  services.xserver.windowManager.dwm.enable = true;

  # Enable the KDE Plasma Desktop Environment.
  services.xserver.displayManager.sddm.enable = true;
  services.xserver.desktopManager.plasma5.enable = true;


  services.picom.enable = true;

  # Configure keymap in X11
  services.xserver = {
    layout = "us";
    xkbVariant = "";
  };

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.shasten = {
    isNormalUser = true;
    description = "shasten";
    extraGroups = [ "networkmanager" "wheel" "kvm" "input" "disk" "libvirtd" "audio" "video" "disk" "dialout" ];
    packages = with pkgs; [
      firefox
      kate
    #  thunderbird
    ];
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
  alacritty
  autojump
  ardour
  brave
  bspwm
  cargo
  carla
  ccacheStdenv
  checkinstall
  clang-tools_9
  clipgrab
  curl
  dmenu
  dwm
  dwmbar
  dwmblocks
  exa
  eva
  fakeroot
  feh
  fontconfig
  freetype
  flameshot
  flatpak
  galculator
  gcc
  gh
  ghc
  gimp
  git
  gitlab
  go
  google-chrome
  gnugrep
  gnumake
  guitarix
  gparted
  hugo
  hydrogen
  jack2
  jitsi
  kdenlive
  kitty
  leafpad
  libffi
  libvirt
  libreoffice
  lsp-plugins
  luarocks
  lxappearance
  mangohud
  meh
  mlocate
  mongoc
  morgen
  mpv
  muse
  neofetch
  neovim
  nerdfonts
  netflix 
  nfs-utils
  ninja 
  nitrogen
  nodejs
  nodePackages.npm
  nsxiv
  ocamlPackages.ssl
  octavePackages.database
  openssl
  pavucontrol
  pkg-config
  pcmanfm
  picom
  pidgin
  pipenv
  polkit_gnome
  polybar
  postgresql_jit
  protonup-ng
  purple-plugin-pack
  python310Packages.virtualenv
  python3Full
  python.pkgs.pip
  qemu
  qjackctl
  qpwgraph
  ranger
  ripgrep
  rofi
  rustc
  rustup
  slack
  spotify-cli-linux
  st
  stdenv
  sxhkd
  terminus-nerdfont
  temurin-jre-bin
  tldr
  unzip
  virt-manager
  virtualenv
  xclip
  xlibinput-calibrator
  xdg-desktop-portal-gtk
  x11basic
  xorg.libX11
  xorg.libX11.dev
  xorg.libxcb
  xorg.libXft
  xorg.libXinerama
  xorg.xinit
  xorg.xinput
  vscode
  wget
  winetricks
  zathura
  zlib
  zoom
  ];

nixpkgs.overlays = [
	(final: prev: {
		dwm = prev.dwm.overrideAttrs (old: { src = /home/shasten/Gitlab/dwm  ;});
	})
  ];

fonts = {
    fonts = with pkgs; [
      noto-fonts
      source-code-pro
      noto-fonts-emoji
      font-awesome
 ];
      fontconfig = {
      enable = true;
      defaultFonts = {
	      monospace = [ "Source Code Pro" ];
	      serif = [ "Noto Serif" "Source Han Serif" ];
	      sansSerif = [ "Noto Sans" "Source Han Sans" ];
      };
    };
};

  system.copySystemConfiguration = true;
  system.autoUpgrade.enable = true;  
  system.autoUpgrade.allowReboot = true; 
  system.autoUpgrade.channel = "https://channels.nixos.org/nixos-23.05";
  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
 # };

  # List services that you want to enable:
  virtualisation.libvirtd.enable = true; 
  # enable flatpak support
  services.dbus.enable = true;
  xdg.portal = {
    enable = true;
  };
  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Enable Postgresql
  services.postgresql.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?

}
